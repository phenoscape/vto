# The Vertebrate Taxonomy Ontology (VTO)

The Vertebrate Taxonomy Ontology includes both extinct and extant vertebrates,
aiming to provide one comprehensive hierarchy. The hierarchy backbone for
extant taxa is based on the NCBI taxonomy. Since the NCBI taxonomy only
includes species associated with archived genetic data, to complement this,
we also incorporate taxonomic information across the vertebrates from the
Paleobiology Database (PaleoDB). The Teleost Taxonomy Ontology (TTO) and
AmphibiaWeb (AWeb) are incorporated to provide a more authoritative hierarchy
and a richer set of names for specific taxonomic groups.

## Citation and terms of reuse

If you use VTO in your work, please cite the following publication:

> Midford P, Dececchi T, Balhoff J, Dahdul W, Ibrahim N, Lapp H, et al. The vertebrate taxonomy ontology: a framework for reasoning across model organism and species phenotypes. J Biomed Semantics. 2013;4: 34. [doi:10.1186/2041-1480-4-34](http://dx.doi.org/10.1186/2041-1480-4-34)

VTO is released under the [CC-Zero](https://creativecommons.org/publicdomain/zero/1.0/) Public Domain waiver. 